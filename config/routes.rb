Rails.application.routes.draw do
  get 'audioroom',   to: 'janus#audioroom'

  root to: 'janus#textroomtest'
end
